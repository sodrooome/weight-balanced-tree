from weight_balanced_tree import WeightBalancedTree


wbt = WeightBalancedTree()
for index in range(5):
    wbt.insert(index)

print(wbt.search(2))
print(wbt.detect_anomalies(threshold=0, features=[1]))  # this features classification may pointless and holds no practical significance

# finding based on the sorted values
values = [9, 13, 8, 2, 5, 7, 24, 19, 20, 54, 109, 84, 1, 6]
for index in sorted(values):
    wbt.insert(index)

print(wbt.search(1))
print(wbt.delete(13))
print(wbt.detect_anomalies(threshold=5, features=[1]))
