from weight_balanced_tree import WeightBalancedTree, HttpRequest


wbt = WeightBalancedTree()
http_request = HttpRequest(url="https://google.com")
for _ in range(15):
    status_code = http_request.send_request()
    if status_code is not None:
        wbt.insert(status_code)
        print(status_code)  # just for debugging, might've removed later on
    else:
        print("There's no status code at all {0}".format(status_code))

list_of_anomalies = [
    400,
    500,
]  # list down the features classification (based on status code) to identify which one is anomaly
print(wbt.search(200))
print(
    wbt.detect_anomalies(5, list_of_anomalies)
)  # no anomaly, since the returned status code supposed to be 200 OK
