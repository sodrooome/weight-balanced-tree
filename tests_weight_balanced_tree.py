import unittest
from weight_balanced_tree import WeightBalancedTree, HttpRequest


class TestWeightBalancedTree(unittest.TestCase):
    def setUp(self) -> None:
        self.wbt = WeightBalancedTree()

        self.insertion = []
        for index in range(25):
            self.insertion = self.wbt.insert(index)

    def test_sorted_wbt(self):
        values = [9, 10, 2, 3, 5, 11, 12, 8, 20]

        for value in sorted(values):
            self.wbt.insert(value)

        self.assertIsNotNone(values)

    def test_insertion_wbt(self):
        self.assertIsNotNone(self.insertion, "Is Not None")

    def test_search_wbt(self):
        self.assertTrue(self.wbt.search(12))
        self.assertFalse(self.wbt.search(999))

    def test_detect_anomalies(self):
        self.assertIsNone(self.wbt.detect_anomalies(1, []))

    def test_find_detect_anomalies(self):
        threshold = 0
        features = [1, 2, 3]
        self.assertRaises(OverflowError, self.wbt.detect_anomalies, threshold, features)

    def test_delete_wbt(self):
        self.assertFalse(self.wbt.delete(1))

    # TODO: write a tests to make a comparison between WBT and BST
    # TODO: write a tests when finding a key in the large of datasets model

    def tearDown(self) -> None:
        return super().tearDown()


if __name__ == "__main__":
    unittest.main()
